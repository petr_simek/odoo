package com.login.odoo.xsimek20.odoologin.db;


import android.content.ContentValues;
import android.database.Cursor;

import java.util.HashMap;

public class AccountEntity {

    private Long id;
    private String db;
    private String url;
    private Integer port;
    private String userName;
    private String password;
    private Integer userId;
    private Integer employeeId;

    public AccountEntity(Cursor cursor) {
        id = cursor.getLong(0);
        port = cursor.getInt(1);
        db = cursor.getString(2);
        url = cursor.getString(3);
        userName = cursor.getString(4);
        password = cursor.getString(5);
        userId = cursor.getInt(6);
        employeeId = cursor.getInt(7);
    }

    public AccountEntity(HashMap<String, String> data) {
        db = data.get("db");
        url = data.get("url");
        try {
            port = Integer.valueOf(data.get("port"));
        } catch (NumberFormatException e) {
            port = 0;
        }
        userName = data.get("userName");
        password = data.get("password");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public ContentValues getValues() {
        ContentValues values = new ContentValues();
        if (port != null)
            values.put("port", port);
        if (db != null)
            values.put("db", db);
        if (url != null)
            values.put("url", url);
        if (userName != null)
            values.put("userName", userName);
        if (userId != null)
            values.put("userId", userId);
        if (employeeId != null)
            values.put("employeeId", employeeId);
        if (password != null)
            values.put("password", password);
        return values;
    }
}
