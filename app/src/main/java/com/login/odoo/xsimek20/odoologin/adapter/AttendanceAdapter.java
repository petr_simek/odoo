package com.login.odoo.xsimek20.odoologin.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.entity.Attendance;

import java.util.List;

/**
 * Created by xsimek20 on 19.12.2015.
 */
public class AttendanceAdapter  extends BaseAdapter {
    private List<Attendance> _AttendanceList;
    private LayoutInflater _inflater;
    private Context context;

    public AttendanceAdapter(Context context, List<Attendance> attendances) {
        _inflater = LayoutInflater.from(context);
        _AttendanceList = attendances;
        this.context = context;
    }

    @Override
    public int getCount() {
        return _AttendanceList.size();
    }

    @Override
    public Attendance getItem(int position) {
        return _AttendanceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if(convertView == null) {
            view = _inflater.inflate(R.layout.attendance_item, parent, false);
            holder = new ViewHolder();
            holder.date = (TextView)view.findViewById(R.id.attendance_date);
            holder.attendance_id = (TextView)view.findViewById(R.id.attendance_id);
            holder.attendance_out = (ImageView) view.findViewById(R.id.attendance_out);
            holder.attendance_in = (ImageView) view.findViewById(R.id.attendance_in);
            holder.layout = (RelativeLayout) view.findViewById(R.id.attendace_item_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }

        Attendance attendance = _AttendanceList.get(position);

        holder.attendance_id.setText(String.valueOf(attendance.getId()));
        holder.date.setText(attendance.getTime());

        holder.attendance_out.setVisibility(attendance.getAction().equals("sign_out")
                ? View.VISIBLE : View.GONE);
        holder.attendance_in.setVisibility(attendance.getAction().equals("sign_in")
                ? View.VISIBLE : View.GONE);

//        holder.layout.setBackgroundColor(context.getResources().getColor(Color.green(attendance.getColor())));
        return view;
    }

    private class ViewHolder {
        //public ImageView avatar;
        public TextView  attendance_id;
        public TextView date;
        public ImageView attendance_out, attendance_in;
        public RelativeLayout layout;
    }
}
