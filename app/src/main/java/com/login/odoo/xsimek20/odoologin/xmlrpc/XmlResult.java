package com.login.odoo.xsimek20.odoologin.xmlrpc;

public class XmlResult {

    private final boolean error;
    private int messageId;
    private Object data;

    public XmlResult(boolean error, int messageId) {
        this.error = error;
        this.messageId = messageId;
    }

    public XmlResult(Object data) {
        this.data = data;
        this.error = false;
    }

    public XmlResult() {
        this.error = false;
    }

    public boolean isError() {
        return error;
    }

    public int getMessageId() {
        return messageId;
    }

    public Object getData() {
        return data;
    }

    public boolean getBoolean() {
        return (boolean)data;
    }
}
