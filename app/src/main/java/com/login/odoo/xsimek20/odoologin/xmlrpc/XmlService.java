package com.login.odoo.xsimek20.odoologin.xmlrpc;

import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.entity.Attendance;
import com.login.odoo.xsimek20.odoologin.entity.AttendanceList;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import de.timroes.axmlrpc.XMLRPCClient;
import de.timroes.axmlrpc.XMLRPCException;

import static java.util.Arrays.asList;

public class XmlService {

    public static final String ABSENT = "absent";
    public static final String PRESENT = "present";

    private AccountEntity acount;
    private final URL loginUrl;
    private final URL methodUrl;



    public XmlService(AccountEntity acount) throws MalformedURLException {
        this.acount = acount;
        loginUrl = new URL("http", acount.getUrl(), acount.getPort(), "/xmlrpc/common");
        methodUrl = new URL("http", acount.getUrl(), acount.getPort(), "/xmlrpc/2/object");
    }

    public XmlResult isLogin() {
        return compareStateWithServer(PRESENT);
    }

    public XmlResult isAbsent() {
        return compareStateWithServer(ABSENT);
    }

    public XmlResult login() {
        XmlResult result = isAbsent();
        if (result.isError()) {
            return result;
        }
        if ((boolean)result.getData()) {
            changeAttendance();
        }
        return result;
    }

    public XmlResult logout() {
        XmlResult result = isLogin();
        if (result.isError()) {
            return result;
        }
        if ((boolean)result.getData()) {
            changeAttendance();
        }
        return result;
    }

    public XmlResult testLoginInformation() {
        XmlResult result = setUserId();
        if (result.isError()) {
            return result;
        }
        result = setEmployeeId();
        if (result.isError()) {
            return result;
        }

        return new XmlResult(acount);
    }

    public XmlResult getAttendance() {
        XmlResult result = setEmployeeId();
        if (result.isError()) {
            return result;
        }

        try {
            return new XmlResult(findAttendanceByIds(findAttendanceIds()));
        } catch (Exception e) {
            return new XmlResult(true, R.string.xml_exception_attendance);
        }
    }

    public XmlResult updaAttendance(Attendance attendance) {
        HashMap<String, Object> map = new HashMap();
        map.put("name", attendance.getTime());
        try {
            getClient(methodUrl).call("execute_kw", getDb(), getUserId(), getPassword(),
                    "hr.attendance", "write", asList(asList(attendance.getId()), map ));
        } catch (Exception e) {
            if (e.getMessage().contains("You cannot modify an entry in a confirmed timesheet")) {
                return new XmlResult(true, R.string.xml_exception_attendance_modify);
            }
            return new XmlResult(true, R.string.xml_exception_attendance);
        }
        return new XmlResult(null);
    }

    private XmlResult setUserId() {
        try {
            Object result = getClient(loginUrl).call("login", getDb(), getUserName(), getPassword());
            if (result instanceof Boolean) {
                return new XmlResult(true, R.string.xmlrpc_login_error);
            } else if (result instanceof Integer) {
                acount.setUserId((Integer)result);
                return new XmlResult(getUserId());
            }
            return new XmlResult(true, R.string.xml_exception_login);
        } catch (Exception e) {
            return new XmlResult(true, R.string.xml_exception_set_user);
        }
    }

    private XmlResult changeAttendance() {
        XmlResult result = setEmployeeId();
        if (result.isError()) {
            return result;
        }

        try {
            return new XmlResult(getClient(methodUrl).call("execute", getDb(), getUserId(), getPassword(),
                    "hr.employee", "attendance_action_change", Collections.singletonList(getEmployeeId())));
        } catch (Exception e) {
           return new XmlResult(true, R.string.xml_exception_attendance);
        }
    }

    private List<Integer> findAttendanceIds() throws XMLRPCException{
        HashMap<String, Object> map = new HashMap();
        map.put("order", "name DESC");
        map.put("limit", 10);
        return convertObjectToIntList((Object[]) getClient(methodUrl).call("execute_kw", getDb(), getUserId(), getPassword(),
                "hr.attendance", "search", Collections.singletonList(Collections.singletonList(
                        asList("employee_id", "=", getEmployeeId()))), map));
    }

    private List<Integer> convertObjectToIntList(Object[] ids) {
        List<Integer> ret = new ArrayList<>(ids.length);

        for (Object id : ids) {
            ret.add((int) id);
        }
        return ret;
    }

    private List<Attendance> findAttendanceByIds(List<Integer> ids) throws XMLRPCException{
        HashMap<String, Object> map = new HashMap();
        map.put("fields", asList("id", "name", "action"));
        Object[] result = (Object[])getClient(methodUrl).call(
                "execute_kw", getDb(), getUserId(), getPassword(), "hr.attendance", "read",
                        asList(ids), map);

        List<Attendance> attendances = new AttendanceList();

        for (Object dataObject : result) {
            Map mapData = (Map)dataObject;
            attendances.add(new Attendance((String)mapData.get("action"), (Integer)mapData.get("id"),
                    (String)mapData.get("name")));
        }
        return attendances;
    }

    private XmlResult compareStateWithServer(String state) {
        if (getUserId() == null) {
            XmlResult result = setUserId();
            if (result.isError()) {
                return result;
            }
        }

        try {
            Object data = getClient(methodUrl).call("execute", getDb(), getUserId(), getPassword(),
                    "hr.employee", "search_read",
                    Collections.singletonList(asList("user_id", "=", getUserId())), new HashMap() {{
                        Collections.singletonList("state");
                    }});
            HashMap map = (HashMap) ((Object[]) data)[0];
            if (map.containsKey("state")) {
                return new XmlResult(map.get("state").toString().startsWith(state));
            } else {
                return new XmlResult(true, R.string.xmlservise_state_dont_exist);
            }
        } catch (Exception e) {
            return new XmlResult(true, R.string.xml_exception);
        }
    }

    private XmlResult setEmployeeId() {
        try {
            Object[] employeeId = (Object[])getClient(methodUrl)
                    .call("execute", getDb(), getUserId(), getPassword(), "hr.employee", "search",
                            Collections.singletonList(asList("user_id", "=", getUserId())));
            acount.setEmployeeId((Integer)employeeId[0]);
            return new XmlResult(getEmployeeId());
        } catch (Exception e) {
            return new XmlResult(true, R.string.xml_exception_set_employe);
        }
    }

    private XMLRPCClient getClient(URL url) {
        return new XMLRPCClient(url);
    }

    public String getDb() {
        return acount.getDb();
    }

    public String getServer() {
        return acount.getUrl();
    }

    public String getPassword() {
        return acount.getPassword();
    }

    public String getUserName() {
        return acount.getUserName();
    }

    public Integer getPort() {
        return acount.getPort();
    }

    public Integer getEmployeeId() {
        return acount.getEmployeeId();
    }

    public Integer getUserId() {
        return acount.getUserId();
    }

}
