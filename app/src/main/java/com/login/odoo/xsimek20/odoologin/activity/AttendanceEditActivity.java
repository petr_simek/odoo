package com.login.odoo.xsimek20.odoologin.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.entity.Attendance;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlService;

import java.net.MalformedURLException;

/**
 * Created by xsimek20 on 20.12.2015.
 */
public class AttendanceEditActivity extends DefaultActivity {

    public static final String NAME_KEY = "attendance.key";
    public static final String NAME_ID = "attendance.id";
    public static final String NAME_ACTION = "attendance.action";

    public TextView  attendance_id;
    public EditText date;
    public ImageView attendance_out, attendance_in;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance_edit);
        Intent intent = getIntent();

        date = (EditText)findViewById(R.id.attendance_date);
        attendance_id = (TextView)findViewById(R.id.attendance_id);
        attendance_out = (ImageView) findViewById(R.id.attendance_out);
        attendance_in = (ImageView) findViewById(R.id.attendance_in);

        attendance_id.setText(String.valueOf(intent.getIntExtra(NAME_ID, 0)));
        date.setText(intent.getStringExtra(NAME_KEY));

        attendance_out.setVisibility(intent.getStringExtra(NAME_ACTION).equals("sign_out")
                ? View.VISIBLE : View.GONE);
        attendance_in.setVisibility(intent.getStringExtra(NAME_ACTION).equals("sign_in")
                ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void saveJourney(View v) {
        Integer attendanceId = Integer.parseInt(attendance_id.getText().toString());
        String date = this.date.getText().toString();
        new UpdateAttendanceTask().execute(new Attendance(attendanceId, date));
        setUpLoadDialog();
    }

    protected void updateEnd() {
        finish();
    }

    protected class UpdateAttendanceTask extends AsyncTask<Attendance, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(Attendance... params) {
            try {
                return new XmlService(getAccount()).updaAttendance(params[0]);
            } catch (MalformedURLException e) {
                return new XmlResult(true, R.string.wrong_url);
            }
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            progressDialog.dismiss();
            if (xmlResult.isError()) {
                sendError(xmlResult.getMessageId());
            } else {
                updateEnd();
            }
        }
    }
}
