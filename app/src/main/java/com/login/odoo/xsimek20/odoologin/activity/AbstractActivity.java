package com.login.odoo.xsimek20.odoologin.activity;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NotificationCompat;

import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlService;

import java.net.MalformedURLException;

public abstract class AbstractActivity extends FragmentActivity {

    public XmlResult loginActivity(AccountEntity account) {
        if (account == null) {
            return new XmlResult(true, R.string.user_data_missing);
        }
        try {
            return new XmlService(account).login();
        } catch (MalformedURLException e) {
            return new XmlResult(true, R.string.wrong_url);
        }
    }

    public XmlResult logoutActivity(AccountEntity account) {
        if (account == null) {
            return new XmlResult(true, R.string.user_data_missing);
        }
        try {
            return new XmlService(account).logout();
        } catch (MalformedURLException e) {
            return new XmlResult(true, R.string.wrong_url);
        }
    }

    public XmlResult isLogin(AccountEntity account) {
        if (account == null) {
            return new XmlResult(true, R.string.user_data_missing);
        }
        try {
            return new XmlService(account).isLogin();
        } catch (MalformedURLException e) {
            return new XmlResult(true, R.string.wrong_url);
        }
    }

    public XmlResult testLoginInformation(AccountEntity account) {
        try {
            return new XmlService(account).testLoginInformation();
        } catch (MalformedURLException e) {
            return new XmlResult(true, R.string.wrong_url);
        }
    }

    protected void sendNotification(int  message) {
        sendNotification(getString(message), message);
    }

    protected void sendNotification(String  message, int messageId) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(getIdOfNotification())
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(message);

        Intent resultIntent = new Intent(this, MainActivity.class);
        PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            resultIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(messageId, mBuilder.build());
    }

    abstract protected int getIdOfNotification();
}
