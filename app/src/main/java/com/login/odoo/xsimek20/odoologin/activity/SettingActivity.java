package com.login.odoo.xsimek20.odoologin.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.db.DbHelper;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;


public class SettingActivity extends DefaultActivity {

    private AccountEntity accountSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_layout);
        AccountEntity account = getAccount();
        if (account != null) {
            setValueForEditText(R.id.s_name, account.getUserName());
            setValueForEditText(R.id.s_password, account.getPassword());
            setValueForEditText(R.id.s_db, account.getDb());
            setValueForEditText(R.id.s_port, account.getPort().toString());
            setValueForEditText(R.id.s_url, account.getUrl());
        }
    }

    public void saveSettingsActivity(View v) {
        HashMap<String, String> data = new HashMap<>();
        data.put("userName",getValueFromEditText(R.id.s_name));
        data.put("password", getValueFromEditText(R.id.s_password));
        data.put("db", getValueFromEditText(R.id.s_db));
        data.put("port", getValueFromEditText(R.id.s_port));
        data.put("url", getValueFromEditText(R.id.s_url));

        for (String val : data.values()) {
            if (StringUtils.isEmpty(val)) {
                sendError(R.string.s_error);
                return;
            }
        }

        accountSetting = new AccountEntity(data);
        setUpLoadDialog();
        new XmlRpcTask().execute();
    }

    private void saveSettingsActivity(XmlResult result) {
        progressDialog.dismiss();
        if (result.isError()) {
            sendError(result.getMessageId());
            return;
        }
        AccountEntity account = getAccount();
        accountSetting.setId(account != null ? account.getId() : null);
        new DbHelper(getApplicationContext()).insertOdoo((AccountEntity) result.getData());
        Intent intent = new Intent();
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    protected String getValueFromEditText(int r) {
        EditText text = (EditText)findViewById(r);
        return text.getText().toString();
    }

    protected void setValueForEditText(int r, String value) {
        EditText text = (EditText)findViewById(r);
        text.setText(value);
    }

    protected class XmlRpcTask extends AsyncTask<AccountEntity, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(AccountEntity... params) {
            return testLoginInformation(accountSetting);
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            saveSettingsActivity(xmlResult);

        }
    }

}
