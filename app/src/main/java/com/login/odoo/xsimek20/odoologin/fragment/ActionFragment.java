package com.login.odoo.xsimek20.odoologin.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.login.odoo.xsimek20.odoologin.activity.MainActivity;
import com.login.odoo.xsimek20.odoologin.db.DbHelper;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.activity.DefaultActivity;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlService;

import java.net.MalformedURLException;


public abstract class ActionFragment extends Fragment {

    public static final String INTENT_KEY_REFRESH = "action.fragment.refresh";
    public static final String INTENT_KEY_REFRESH_ERROR = "action.fragment.refresh.error";

    SwipeRefreshLayout mSwipeRefreshLayout;

    abstract protected int getLayoutId();
    abstract protected int getSwipeId();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(getSwipeId());
        mSwipeRefreshLayout.setColorSchemeColors(
                R.color.swipe_color_1, R.color.swipe_color_2,
                R.color.swipe_color_3, R.color.swipe_color_4);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initiateRefresh();
            }
        });
    }

    private void initiateRefresh() {
        new RefreshTask().execute(new DbHelper(getActivity().getApplicationContext()).getAccount());
    }

    private void onRefreshComplete(XmlResult result) {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        if (result.isError()) {
            intent.putExtra(INTENT_KEY_REFRESH_ERROR, result.getMessageId());
            intent.setAction("error");
        } else {
            intent.putExtra(INTENT_KEY_REFRESH, result.getBoolean());
            intent.setAction("ok");
        }
        mSwipeRefreshLayout.setRefreshing(false);
        startActivity(intent);
    }

    private class RefreshTask extends AsyncTask<AccountEntity, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(AccountEntity... params) {
            if (params[0] == null) {
                return new XmlResult(true, R.string.user_data_missing);
            }
            try {
                return new XmlService(params[0]).isLogin();
            } catch (MalformedURLException e) {
                return new XmlResult(true, R.string.wrong_url);
            }
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            onRefreshComplete(xmlResult);
        }
    }

}
