package com.login.odoo.xsimek20.odoologin.db;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Odoo.Attendance.db";
    public static final String ODOO_TABLE = "account";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ODOO_TABLE
                    + " ( id INTEGER PRIMARY KEY ," +
                    " port INTEGER," +
                    " db TEXT," +
                    " url TEXT," +
                    " userName TEXT," +
                    " password TEXT," +
                    " userId INTEGER," +
                    " employeeId INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ODOO_TABLE;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public AccountEntity insertOdoo(AccountEntity account) {
        SQLiteDatabase db =  this.getWritableDatabase();
        if (account.getId() != null) {
            String[] selectionArgs = { String.valueOf(account.getId()) };
            db.update(ODOO_TABLE, account.getValues(), "ID LIKE ?", selectionArgs);
        } else {
            account.setId(db.insert(ODOO_TABLE, "userId, employeeId", account.getValues()));
        }
        return account;
    }

    public AccountEntity getAccount() {
        SQLiteDatabase db =  this.getWritableDatabase();
        Cursor c = db.query(
                ODOO_TABLE,  // The table to query
                new String[]{"id, port, db, url, username, password, userId, employeeId"},
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                    null                             // The sort order
        );
        if (c.moveToFirst()) {
            return new AccountEntity(c);
        }
        return null;
    }
}
