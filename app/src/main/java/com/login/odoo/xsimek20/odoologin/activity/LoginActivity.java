package com.login.odoo.xsimek20.odoologin.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.login.odoo.xsimek20.odoologin.db.DbHelper;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;

import java.util.Calendar;

public class LoginActivity extends AbstractActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LoginTask().execute(new DbHelper(getApplicationContext()).getAccount());
    }

    public void loginFinish(XmlResult result) {
        Intent intent = new Intent();
        if (result.isError()) {
            sendNotification(result.getMessageId());
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
            return;
        }
        if (result.getBoolean()) {
            sendNotification(getString(R.string.login_ok, Calendar.getInstance().getTime()),
                    R.string.login_ok);
        } else {
            sendNotification(R.string.login_not);
        }
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    protected int getIdOfNotification() {
        return R.drawable.ic_stat_login_notification;
    }

    private class LoginTask extends AsyncTask<AccountEntity, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(AccountEntity... params) {
                return loginActivity(params[0]);
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            loginFinish(xmlResult);
        }
    }
}
