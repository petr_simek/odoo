package com.login.odoo.xsimek20.odoologin.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.db.DbHelper;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;

import java.util.Calendar;

public class LogoutActivity extends AbstractActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LogoutTask().execute(new DbHelper(getApplicationContext()).getAccount());
    }

    private void logoutFinish(XmlResult result) {
        Intent intent = new Intent();
        if (result.isError()) {
            sendNotification(result.getMessageId());
            setResult(Activity.RESULT_CANCELED, intent);
            finish();
            return;
        }
        if (result.getBoolean()) {
            sendNotification(getString(R.string.logout_ok, Calendar.getInstance().getTime()),
                    R.string.logout_ok);
        } else {
            sendNotification(R.string.logout_not);
        }
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    protected int getIdOfNotification() {
        return R.drawable.ic_stat_logout_notification;
    }

    private class LogoutTask extends AsyncTask<AccountEntity, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(AccountEntity... params) {
            return logoutActivity(params[0]);
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            logoutFinish(xmlResult);
        }
    }
}
