package com.login.odoo.xsimek20.odoologin.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

public class Attendance implements Serializable {

    private String action;
    private Integer id;
    private String time;

    public Attendance(Integer id, String time) {
        this.id = id;
        this.time = time;
    }

    public Attendance(String action, Integer id, String time) {
        this.action = action;
        this.id = id;
        this.time = time;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "{" +
                "action='" + action + '\'' +
                ", id=" + id +
                ", time='" + time + '\'' +
                '}';
    }


}
