package com.login.odoo.xsimek20.odoologin.activity;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.db.DbHelper;
import com.login.odoo.xsimek20.odoologin.fragment.ActionFragment;
import com.login.odoo.xsimek20.odoologin.fragment.ErroFragment;
import com.login.odoo.xsimek20.odoologin.fragment.LoginFragment;
import com.login.odoo.xsimek20.odoologin.fragment.LogoutFragment;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;

public class MainActivity extends DefaultActivity  {
    protected int methodId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFragment(new ErroFragment());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (getAccount() == null) {
            showSettings();
            return;
        }
        setUpLoadDialog();
        methodId = 3;
        new XmlRpcTask().execute();
    }

    @Override
    public void startActivityFromFragment(Fragment fragment, Intent intent, int requestCode) {
        super.startActivityFromFragment(fragment, intent, requestCode);
        if (intent.getAction().startsWith("ok")) {
            setFragment(intent.getBooleanExtra(ActionFragment.INTENT_KEY_REFRESH, false)
                    ? new LogoutFragment() : new LoginFragment());
        } else {
            sendError(intent.getIntExtra(ActionFragment.INTENT_KEY_REFRESH,
                    R.string.xmlrpc_login_error));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.settings:
                showSettings();
                return true;
            case R.id.showAttendance:
                Intent intent = new Intent(getApplicationContext(), ShowAttendanceActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loginActivity(View v) {
        setUpLoadDialog();
        methodId = 1;
        new XmlRpcTask().execute();
    }

    public void logoutActivity(View v) {
        setUpLoadDialog();
        methodId = 2;
        new XmlRpcTask().execute();
    }

    public void refreshActivity(View v) {
        setUpLoadDialog();
        methodId = 3;
        new XmlRpcTask().execute();
    }

    public void deleteDbActivity(View v) {
        DbHelper db = new DbHelper(getApplicationContext());
        db.onUpgrade(db.getWritableDatabase(), 1, 1);
    }

    private void loginAfterActivity(XmlResult result) {
        progressDialog.dismiss();
        if (result.isError()) {
            sendError(result.getMessageId());
            return;
        }
        setFragment(new LogoutFragment());
    }

    private void logoutAfterActivity(XmlResult result) {
        progressDialog.dismiss();
        if (result.isError()) {
            sendError(result.getMessageId());
            return;
        }
        setFragment(new LoginFragment());
    }

    private void refreshAfterActivity(XmlResult result) {
        progressDialog.dismiss();
        if (result.isError()) {
            setFragment(new ErroFragment());
            sendError(result.getMessageId());
            return;
        }
        setFragment(result.getBoolean() ? new LogoutFragment() : new LoginFragment());
    }

    protected void setFragment(ActionFragment fragment) {
        setContentView(R.layout.defautl_layout);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.sample_content_fragment, fragment);
        transaction.commit();
    }

    public int getMethodId() {
        return methodId;
    }

    protected class XmlRpcTask extends AsyncTask<AccountEntity, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(AccountEntity... params) {
            switch (getMethodId()) {
                case 1:
                    return loginActivity(getAccount());
                case 2:
                    return logoutActivity(getAccount());
                default:
                    return isLogin(getAccount());
            }
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            switch (getMethodId()) {
                case 1:
                    loginAfterActivity(xmlResult);
                    break;
                case 2:
                    logoutAfterActivity(xmlResult);
                    break;
                default:
                    refreshAfterActivity(xmlResult);
            }
        }
    }
}
