package com.login.odoo.xsimek20.odoologin.activity;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.db.DbHelper;

public abstract class DefaultActivity extends AbstractActivity {

    protected ProgressDialog progressDialog;

    protected void showSettings() {
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }

    public void deleteDbActivity(View v) {
        DbHelper db = new DbHelper(getApplicationContext());
        db.onUpgrade(db.getWritableDatabase(), 1, 1);
    }

    protected void sendError(int errorId) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getResources().getString(R.string.error_dialog_label));
        alertDialog.setMessage(getResources().getString(errorId));
        alertDialog.setButton(getResources().getString(R.string.error_dialog_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();

    }

    public AccountEntity getAccount() {
        DbHelper db = new DbHelper(getApplicationContext());
        return db.getAccount();
    }

    @Override
    protected int getIdOfNotification() {
        return R.drawable.ic_stat_login_notification;
    }

    protected void setUpLoadDialog() {
        progressDialog = ProgressDialog.show(this, getString(R.string.dialog_loading_label),
                getString(R.string.dialog_loading_text), false, false);
        progressDialog.show();
    }
}
