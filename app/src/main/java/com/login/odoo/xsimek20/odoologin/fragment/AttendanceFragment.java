package com.login.odoo.xsimek20.odoologin.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.activity.AttendanceEditActivity;
import com.login.odoo.xsimek20.odoologin.adapter.AttendanceAdapter;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.db.DbHelper;
import com.login.odoo.xsimek20.odoologin.entity.Attendance;
import com.login.odoo.xsimek20.odoologin.entity.AttendanceList;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlService;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class AttendanceFragment extends ListFragment {

    private AttendanceList data;
    private boolean active = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private static final String DESCRIBABLE_KEY = "describable_key";

    public static AttendanceFragment newInstance(AttendanceList data) {
        AttendanceFragment fragment = new AttendanceFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, data);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            data = (AttendanceList) getArguments().getSerializable(
                    DESCRIBABLE_KEY);
            AttendanceAdapter adapter = new AttendanceAdapter(getContext(), data);
            setListAdapter(adapter);
        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }



    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Integer attendanceId = Integer.parseInt(((TextView) v.findViewById(R.id.attendance_id)).getText().toString());
        String date = ((TextView) v.findViewById(R.id.attendance_date)).getText().toString();
        Intent intent = new Intent(getActivity(), AttendanceEditActivity.class);
        intent.putExtra(AttendanceEditActivity.NAME_KEY, date);
        intent.putExtra(AttendanceEditActivity.NAME_ID, attendanceId);
        intent.putExtra(AttendanceEditActivity.NAME_ACTION,
                ((ImageView) v.findViewById(R.id.attendance_in)).getVisibility() == View.GONE
                        ? "sign_in" : "sign_out");
        startActivity(intent);
    }

    public AccountEntity getAccount() {
        DbHelper db = new DbHelper(getActivity());
        return db.getAccount();
    }
}
