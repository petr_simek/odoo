package com.login.odoo.xsimek20.odoologin.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.login.odoo.xsimek20.odoologin.R;
import com.login.odoo.xsimek20.odoologin.db.AccountEntity;
import com.login.odoo.xsimek20.odoologin.entity.Attendance;
import com.login.odoo.xsimek20.odoologin.entity.AttendanceList;
import com.login.odoo.xsimek20.odoologin.fragment.AttendanceFragment;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlResult;
import com.login.odoo.xsimek20.odoologin.xmlrpc.XmlService;

import java.net.MalformedURLException;
import java.util.List;

public class ShowAttendanceActivity extends DefaultActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance_layout);
        new ShowAttendanceTask().execute();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected void updateFragment(AttendanceList attendances) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = AttendanceFragment.newInstance(attendances);
        ft.replace(R.id.car_fragment, fragment);
        ft.commit();
    }

    protected class ShowAttendanceTask extends AsyncTask<AccountEntity, Void, XmlResult> {
        @Override
        protected XmlResult doInBackground(AccountEntity... params) {
            try {
                return new XmlService(getAccount()).getAttendance();
            } catch (MalformedURLException e) {
                return new XmlResult(true, R.string.wrong_url);
            }
        }

        @Override
        protected void onPostExecute(XmlResult xmlResult) {
            super.onPostExecute(xmlResult);
            if (xmlResult.isError()) {
                sendError(xmlResult.getMessageId());
            } else {
                updateFragment((AttendanceList) xmlResult.getData());
            }

        }
    }
}
