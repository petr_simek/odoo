package com.login.odoo.xsimek20.odoologin.fragment;

import com.login.odoo.xsimek20.odoologin.R;

public class LoginFragment extends ActionFragment {

    private final int layoutId = R.layout.login_layout;
    private final int swipeId = R.id.activity_main_swipe_refresh_login;

    @Override
    protected int getLayoutId() {
        return layoutId;
    }

    @Override
    protected int getSwipeId() {
        return swipeId;
    }
}
