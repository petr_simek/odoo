package com.login.odoo.xsimek20.odoologin.fragment;


import com.login.odoo.xsimek20.odoologin.R;

public class LogoutFragment extends ActionFragment {

    private final int layoutId = R.layout.logout_layout;
    private final int swipeId = R.id.activity_main_swipe_refresh_logout;

    @Override
    protected int getLayoutId() {
        return layoutId;
    }

    @Override
    protected int getSwipeId() {
        return swipeId;
    }
}
